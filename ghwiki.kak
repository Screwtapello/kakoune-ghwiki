# Declare an option to store our install path.
declare-option -hidden str ghwiki_install_path %sh{dirname "$kak_source"}

# Declare an option to store page-name completions
declare-option -hidden completions ghwiki_completions

define-command \
    -hidden \
    -docstring "Select the target of the wiki link under the cursor." \
    ghwiki-select-link-target \
%{
    try %{
        # Do we already have a wiki link completely selected?
        execute-keys s\A\[\[.*\]\]\z<ret>

    } catch %{
        # No, but maybe if we select the square brackets around us,
        # we'll have a complete wiki link?
        execute-keys <a-a>r
        execute-keys s\A\[\[.*\]\]\z<ret>

    } catch %{
        # OK, we've selected text inside one set of square brackets,
        # and that's not a wiki link,
        # but maybe the second set of square brackets is around *that*?
        execute-keys <a-a>r
        execute-keys s\A\[\[.*\]\]\z<ret>

    } catch %{
        # Nope, no idea.
        fail No wiki link found.
    }

    # We've selected the entire link,
    # now just select the text inside it.
    execute-keys <a-i>r <a-i>r

    # FIXME: If all of our selected page names use the vertical bar syntax,
    # or none of them do, then we'll have all our page names selected.
    # Otherwise, only the page names with vertical bars will still be selected
    # after this.

    try %{
        # If a wiki link contains a vertical bar,
        # the actual target is the text after the last occurrence.
        execute-keys s(?<lt>=\|).*<ret>

        # If the link doesn't contain a vertical bar,
        # then the entire content is the target.
    }
}

define-command \
    -docstring "Follow the wiki link under the main cursor" \
    ghwiki-follow-link \
%{
    # Save the 'a' register so we have a temporary storage space.
    evaluate-commands -save-regs a %{

        # In a draft context (so we don't mess up the user's selections)
        evaluate-commands -draft %{
            # ...select the target page name of the link surrounding each cursor
            ghwiki-select-link-target

            # Copy all the page names to a new scratch buffer
            execute-keys '"ay'
            edit -scratch
            execute-keys '"a<a-p>'

            # Save the selections (each page name selected individually
            execute-keys '"aZ'

            # Try to encode any non-filename-safe characters in the page name.
            try %{ execute-keys "s[<space>/]<ret>c-<esc>" }

            # Restore the selections so each encoded page name is selected.
            execute-keys '"az'

            # Yank all the encoded page names into a register.
            execute-keys '"ay'

            # Discard our scratch buffer.
            delete-buffer
        }

        # Now all our encoded page names are in register a,
        # so let's open up the corresponding files!
        evaluate-commands %sh{
            . "$kak_opt_ghwiki_install_path"/lib.sh

            path=$(dirname "$kak_buffile")
            kakquote edit -- "$(page_to_file "$path" "$kak_main_reg_a")"
            echo
        }
    }
}

define-command \
    -hidden \
    ghwiki-populate-completions \
%{
    evaluate-commands %sh{
        . "$kak_opt_ghwiki_install_path"/lib.sh

        path=$(dirname "$kak_buffile")

        { find_link_sources "$path"; find_link_targets "$path"; } |
            sort -u |
            while read -r link; do
            kakquote set-option -add window ghwiki_completions \
                "$link]]||$link]]"
            echo
        done
    }
}

define-command \
    -docstring "Enable auto-completion of wiki links in the current window." \
    ghwiki-enable-completions \
%{
    # Insert our completion option at the front of the list.
    # Because our completions have non-word characters like spaces and [[]],
    # our completions need to be higher priority to make them show up reliably.
    set-option window completers option=ghwiki_completions %opt{completers}

    hook window InsertIdle .* %{
        try %{
            evaluate-commands -draft -save-regs a %{
                # We only care about the main selection for completion purposes.
                execute-keys <,>

                # Store the selection, so we can restore it
                # for each kind of syntax we try.
                execute-keys '"a' Z

                try %{
                    # Restore the saved selection.
                    execute-keys '"a' z

                    # Is the cursor after a |, but not after ]?
                    execute-keys <a-t>|<a-K>\]<ret>

                    # Is the | after a [ but not after [?
                    execute-keys <a-t>[<a-K>\]<ret>

                    # Does it start with *two* squore brackets?
                    execute-keys HH<a-k>\A\[\[<ret>

                    # Then our completions starts just after the pipe,
                    # which should be the current anchor.
                    execute-keys <a-:><semicolon>

                } catch %{
                    # Restore the saved selection.
                    execute-keys '"a' z

                    # Is the cursor somewhere inside square brackets?
                    execute-keys <a-t>[<a-K>\]<ret>

                    # Does it start with *two* square brackets?
                    execute-keys hH<a-k>\[\[<ret>

                    # Then our completions starts just after
                    # the square brackets.
                    execute-keys ll
                } catch %{
                    fail not-in-a-link
                }

                # Initialise our completions for the current position...
                set-option window ghwiki_completions \
                    "%val{cursor_line}.%val{cursor_column}@%val{timestamp}"

                # ...and fill them out!
                ghwiki-populate-completions
            }
        } catch %{
            evaluate-commands %sh{
                if [ "$kak_error" != "not-in-a-link" ]; then
                    # This is some kind of error we care about!
                    echo fail "$kak_error"
                fi
            }
        }
    }
}

define-command \
    -docstring "
        Enable wiki interactions for this window.

        Interactions include <ret> for following wiki links,
        and auto-completing page names.
        " \
    ghwiki-enable \
%{
    map -docstring "Navigate to the linked page" \
        window normal <ret> ': ghwiki-follow-link<ret>'
    map -docstring "Wiki commands" \
        window user w ': enter-user-mode ghwiki<ret>'
    add-highlighter window/ghwiki-wiki-links regex \[\[[^\]\[]+\]\] 0:link
    ghwiki-enable-completions
}

define-command \
    -docstring "Display a list of pages that link to this one." \
    ghwiki-inbound-links \
%{
    evaluate-commands %sh{
        . "$kak_opt_ghwiki_install_path"/lib.sh

        wikipath=$(dirname "$kak_buffile")
        current_page=$(file_to_page "$kak_buffile")
        current_page_escaped=$(printf %s "$current_page" |
            sed 's/[.*^$[]/\\&/g')
        current_page_regex='\(\[\[\||\)'"$current_page_escaped"']]'

        tempdir=$(mktemp -d "${TMPDIR:-/tmp}"/kak-ghwiki.XXXXXXXX)
        mkfifo "$tempdir/fifo"
        (
            {
                printf "Pages that link to [[$current_page]]:\n\n"
                grep -Rl "$current_page_regex" "$wikipath" |
                    while read -r file; do
                        echo "[[$(file_to_page "$file")]]"
                    done
            } >"$tempdir/fifo"
            rm -rf $tempdir
        ) >/dev/null 2>&1 &

        printf '%s\n' "
            evaluate-commands -try-client $(kakquote "$kak_opt_toolsclient") %{
                $(kakquote edit! -fifo "$tempdir/fifo" "*links*")
                ghwiki-enable
            }
        "
    }
}

define-command \
    -docstring "Display a list of pages that nothing links to." \
    ghwiki-orphan-pages \
%{
    evaluate-commands %sh{
        . "$kak_opt_ghwiki_install_path"/lib.sh
        wikipath=$(dirname "$kak_buffile")
        tempdir=$(mktemp -d "${TMPDIR:-/tmp}"/kak-ghwiki.XXXXXXXX)
        mkfifo "$tempdir/sources"
        mkfifo "$tempdir/targets"
        mkfifo "$tempdir/output"
        (
            find_link_sources "$wikipath" > "$tempdir/sources" 2>&1 &
            find_link_targets "$wikipath" > "$tempdir/targets" 2>&1 &
            {
                printf "Pages with no incoming links:\n\n"
                comm -13 "$tempdir/sources" "$tempdir/targets" |
                    while read -r file; do
                        echo "[[$(file_to_page "$file")]]"
                    done
            } >"$tempdir/output"
            rm -rf $tempdir
        ) >/dev/null 2>&1 &

        printf '%s\n' "
            evaluate-commands -try-client $(kakquote "$kak_opt_toolsclient") %{
                $(kakquote edit! -fifo "$tempdir/output" "*orphans*")
                ghwiki-enable
            }
        "
    }
}

define-command \
    -docstring "Display a list of linked pages that don't exist." \
    ghwiki-missing-pages \
%{
    evaluate-commands %sh{
        . "$kak_opt_ghwiki_install_path"/lib.sh
        wikipath=$(dirname "$kak_buffile")
        tempdir=$(mktemp -d "${TMPDIR:-/tmp}"/kak-ghwiki.XXXXXXXX)
        mkfifo "$tempdir/sources"
        mkfifo "$tempdir/targets"
        mkfifo "$tempdir/output"
        (
            find_link_sources "$wikipath" > "$tempdir/sources" 2>&1 &
            find_link_targets "$wikipath" > "$tempdir/targets" 2>&1 &
            {
                printf "Pages linked to that don't yet exist:\n\n"
                comm -23 "$tempdir/sources" "$tempdir/targets" |
                    while read -r file; do
                        echo "[[$(file_to_page "$file")]]"
                    done
            } >"$tempdir/output"
            rm -rf $tempdir
        ) >/dev/null 2>&1 &

        printf '%s\n' "
            evaluate-commands -try-client $(kakquote "$kak_opt_toolsclient") %{
                $(kakquote edit! -fifo "$tempdir/output" "*missing*")
                ghwiki-enable
            }
        "
    }
}

declare-user-mode ghwiki

map -docstring "What links here?" global ghwiki i ': ghwiki-inbound-links<ret>'
map -docstring "List Orphan pages" global ghwiki o ': ghwiki-orphan-pages<ret>'
map -docstring "List Missing pages" global ghwiki m ': ghwiki-missing-pages<ret>'
