This directory is an example of a wiki in GitHub format.

Put the cursor on one of the links below
and run `:wiki-follow-link` to follow it,
or just hit Enter if you have that mapping defined.

People
======

- [[Frodo Baggins]]
- [[Samwise Gamgee]]
- [[Barliman Butterbur]]


Towns
=====

- [[Hobbiton]]
- [[Bree]]
- [[The last homely house|Rivendell]]
