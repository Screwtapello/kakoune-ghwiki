#!/bin/sh

kakquote() {
    for text; do
        printf "'"
        while true; do
            case "$text" in
                *\'*)
                    printf "%s''" "${text%%\'*}"
                    text=${text#*\'}
                    ;;
                *)
                    printf "%s' " "$text"
                    break
                    ;;
            esac
        done
    done
}

shquote() {
    for text; do
        printf "'"
        while true; do
            case "$text" in
                *\'*)
                    printf "%s'\"'\"'" "${text%%\'*}"
                    text=${text#*\'}
                    ;;
                *)
                    printf "%s' " "$text"
                    break
                    ;;
            esac
        done
    done
}

WIKI_EXTENSIONS="
    md
    mkd
    mkdn
    mdown
    markdown
    textile
    rdoc
    org
    creole
    rst
    rest
    asciidoc
    mediawiki
    wiki
    pod
    txt
"

page_find_expr() {
    IFS=$(printf ' \t\n') set -- $WIKI_EXTENSIONS

    shquote \( -name "*.$1"
    shift
    for ext; do
        shquote -o -name "*.$ext"
    done

    shquote \)
}

page_to_file() {
    path="$1"
    page="$2"
    filebase=$(printf "%s" "$page" | sed 's,[ /],-,g')

    for ext in $WIKI_EXTENSIONS; do
        candidate=$path/$filebase.$ext

        test -f "$candidate" && printf "%s" "$candidate" && return
    done

    # No existing file for this name, default to Markdown.
    printf "%s" "$path/$filebase.md"
}

file_to_page() {
    filename=${1##*/}
    filebase=${filename%.*}

    printf "%s" "$filebase" | sed 's,-, ,g'
}

find_link_sources() {
    # Grab anything that looks like a MediaWiki link.
    grep -hoR '\[\[[^[]*\]\]' "$1" |
        sed '
            # Trim the square brackets,
            # since they are not part of the page name.
            s/^\[\[//
            s/\]\]$//

            # If this is a masked link, remove the mask text
            s/.*|//g

            # We want these page names to be comparable
            # to ones generated from filenames.
            # Spaces and slashes in a link
            # are replaced with - in a filename,
            # and - in a filename is replaced with space in a page name,
            # so this simulates a round-trip.
            s,/, ,g
        ' |
        sort -u
}

find_link_targets() {
    eval find "$(shquote "$1")" -type f "$(page_find_expr)" |
        while read -r file; do
            printf "%s\n" "$(file_to_page "$file")"
        done |
        sort -u
}
