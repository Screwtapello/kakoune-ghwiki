GitHub Wiki-mode for Kakoune
============================

Out of the box,
Kakoune is pretty good at editing text documents,
including formats like Markdown and Asciidoc.
However,
sometimes a single document is not enough
and you need a collection of documents with links between them.
There's a variety of tools
that will turn a directory of Markdown files (or whatever)
into a website,
but it can be a hassle to edit such a directory in Kakoune.

This plugin adds various conveniences
that make editing a wiki more pleasant.

Features
========

  - Open the file that corresponds to
    the wiki link under the cursor
  - Auto-complete page names after typing `[[`
  - List all pages that link to the current page


Installation
============

 1. Make the directory `~/.config/kak/autoload/`
    if it doesn't already exist.

        mkdir -p ~/.config/kak/autoload

 2. If it didn't already exist,
    create a symlink inside that directory
    pointing to Kakoune's default autoload directory
    so it Kakoune will still be able to find
    its default configuration.
    You can find Kakoune's runtime autoload directory
    by typing

        :echo %val{runtime}/autoload

    inside Kakoune.
 3. Put a copy of `wiki.kak` inside
    the new autoload directory,
    such as by checking out this git repository:

        cd ~/.config/kak/autoload/
        git clone https://gitlab.com/Screwtapello/kakoune-ghwiki.git

Configuration
=============

This plugin does not current define any options.

Commands
========

This plugin is not specific to a particular file type
or markup syntax,
so it doesn't make sense to provide default hooks.
Instead,
it provides commands so you can set up your own mappings.

Some of these commands are available from the `ghwiki` user-mode,
so you don't have to memorise all of them.


`ghwiki-enable`
-------------

When invoked, this command sets up wiki-style interactions
for the current window.
In particular, it:

  - Maps the `<ret>` key in normal mode
    to call `ghwiki-follow-link` (described above)
  - Maps `w` in user-mode to enter the `ghwiki` custom user mode.
  - Calls `ghwiki-enable-completions`

`ghwiki-enable-completions`
---------------------------

When invoked,
this command installs an `InsertIdle` hook in the current window
to automatically complete available page names
after you type `[[`, or after `|` inside `[[`.

`ghwiki-follow-link`
------------------

If the main cursor is inside a MediaWiki style link,
(like `[[SomePage]]` or `[[a renamed link|SomePage]]`)
we find the link target
("SomePage" in both those examples)
and edit the corresponding file on disk.

For example,
if you're editing the file `/path/to/wiki/Home.md`,
and you put the main cursor on the link `[[SomePage]]`
and invoke `ghwiki-follow-link`,
the file `/path/to/wiki/SomePage.md` will be opened.

`ghwiki-incoming-links`
---------------------

Assuming the current buffer contains a wiki-page,
open a new buffer listing all the other pages in the same directory
which link to this one.

For example,
if you're editing `foo.md`
and `bar.md` contains `[[foo]]`,
when you run `ghwiki-incoming-links`
then `[[bar]]` will be listed as an incoming link.

Usage
=====

Because GitHub wikis support many markup syntaxes,
this plugin does not enable any features by default.
However,
you might want to add a snippet like this to your `kakrc`:

    hook global WinSetOption filetype=markdown %{
        evaluate-commands %sh{
            if [ -f "$(dirname "$kak_buffile")"/Home.md ]; then
                # This directory might be a GitHub wiki.
                echo "ghwiki-enable"
            fi
        }
    }

Now, when you edit a Markdown file
that looks like it might be part of a GitHub wiki,
pressing Enter on a wiki link
will take you to the page it points to,
and you should get page-name completions
after typing `[[`.

TODO
====

  - Support alternate schemes for flattening awkward characters in page names.
      - Python-Markdown's wiki link support
        replaces spans of spaces with a single underscore
        (or nothing, if there's already an adjacent underscore).
  - When following a link to a page that doesn't exist,
    if the page name begins with a capitalised word
    (like "PersonJoeBloggs" or "City Townsville")
    and a template page for that prefix exists
    (like "PersonTemplate" or "City Template"),
    read the template content into the new buffer,
      - If possible, leave the buffer unmodified.
  - Support for non-MediaWiki wiki-link styles.
      - GitLab wikis use ordinary Markdown links,
        but rewrites the URLs so `/` means the root of the wiki.
      - eraserhd [says][asciidoc-wiki]
        "...since AsciiDoc allows definition of new macros,
        it should be easy to make `wiki:SomePage[]`."
  - Make `ghwiki-inbound-links` take the page name as an optional argument,
    and set up page-name completion for it
  - A `ghwiki-broken-links` command
    that finds links in pages that have no corresponding file on disk.
  - A `ghwiki-orphan-links` command
    that finds pages on disk that are not linked from any page.
  - Highlight MediaWiki links as links?

[asciidoc-wiki]: https://discuss.kakoune.com/t/has-anyone-made-a-wiki-in-kakoune/789/2
